package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private int jumlahMataKuliah;
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;

    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
        this.totalSKS = 0;
        this.jurusan = this.getJurusan(npm);
        this.jumlahMataKuliah = 0;
    }

    public String getJurusan(long npm){
        npm /= 10000000000L;
        if(npm % 100 == 01){
            return "Ilmu Komputer";
        }else{
            return "Sistem Informasi";
        }
    }

    public String getKodeJurusan(long npm){
        npm /= 10000000000L;
        if(npm % 100 == 01){
            return "IK";
        }else{
            return "SI";
        }
    }

    public int getIndex(MataKuliah mataKuliah){
        for(int i = 0 ; i < this.jumlahMataKuliah ; i ++){
            if(this.mataKuliah[i].getNama().equals(mataKuliah.getNama()))
                return i;
        }
        return -1;
    }

    public boolean isMatkulAda(MataKuliah mataKuliah){
        if(getIndex(mataKuliah) != -1){
            System.out.printf("[DITOLAK] %s telah diambil sebelumnya.",mataKuliah.getNama());
            return true;
        }
        return false;
    }

    public boolean isMatkulFull(){
        if(this.jumlahMataKuliah == 10){
            System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
            return true;
        }
        return false;
    }



    public void addMatkul(MataKuliah mataKuliah){
        this.mataKuliah[this.jumlahMataKuliah] = mataKuliah;
        this.jumlahMataKuliah += 1;
        this.totalSKS += mataKuliah.getSks();
    }

    public void dropMatkul(MataKuliah mataKuliah){
        int index = getIndex(mataKuliah);
        if(index == 9){
            this.mataKuliah[9] = null;
        }else{
            for(int i = index ;i < this.jumlahMataKuliah ; i ++)
                this.mataKuliah[i] = this.mataKuliah[i + 1];
        }
        this.jumlahMataKuliah -=1;
        this.totalSKS -= mataKuliah.getSks();
    }


    public void cekIRS(){
        try {
            masalahIRS = new String[100];
            int problem = 0;
            if (this.totalSKS > 24) {
                masalahIRS[problem] = "SKS yang Anda ambil lebih dari 24";
                problem += 1;
            }

            String kodeMahasiswa = this.getKodeJurusan(this.npm);
            for (int i = 0; i < this.getJumlahMataKuliah(); i++) {
                MataKuliah mataKuliah = this.mataKuliah[i];
                String kodeMatkul = mataKuliah.getKode();
                if (kodeMatkul.equals("IK")) {
                    if (!kodeMahasiswa.equals(kodeMatkul)) {
                        masalahIRS[problem] = String.format("Mata Kuliah %s tidak dapat diambil jurusan %s", mataKuliah.getNama(), kodeMahasiswa);
                        problem += 1;
                    }
                } else if (kodeMatkul.equals("SI")) {
                    if (!kodeMahasiswa.equals(kodeMatkul)) {
                        masalahIRS[problem] = String.format("Mata Kuliah %s tidak dapat diambil jurusan %s", mataKuliah.getNama(), kodeMahasiswa);
                        problem += 1;
                    }
                }
            }

            if (problem == 0) {
                System.out.println("IRS tidak bermasalah.");
            } else {
                for (int i = 0; i < problem; i++) {
                    System.out.println(masalahIRS[i]);
                }
            }
        }catch (Exception e){
            System.out.println(e);
        }
    }

    public String toString() {
        return this.getNama();
    }


    /* Getter */

    public MataKuliah[] getMataKuliah() {
        return mataKuliah;
    }

    public int getJumlahMataKuliah() {
        return jumlahMataKuliah;
    }

    public String[] getMasalahIRS() {
        return masalahIRS;
    }

    public int getTotalSKS() {
        return totalSKS;
    }

    public String getNama() {
        return nama;
    }

    public String getJurusan() {
        return jurusan;
    }

    public long getNpm() {
        return npm;
    }
}
