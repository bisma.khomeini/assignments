package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int jumlahMahasiswa;


    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[this.kapasitas];
        this.jumlahMahasiswa = 0;
    }

    public int getIndex(Mahasiswa mahasiswa){
        for(int i = 0 ; i < this.jumlahMahasiswa ;i ++){
            if(this.daftarMahasiswa[i].getNama().equals(mahasiswa.getNama()))
                return i;
        }
        return -1;
    }

    public boolean isMahasiswaFull(){
        if(this.jumlahMahasiswa == this.kapasitas){
            System.out.printf("[DITOLAK] %s telah penuh kapasitasnya.",this.getNama());
            return true;
        }
        return false;
    }

    public boolean isMahasiswaAda(Mahasiswa mahasiswa){
        if(this.getIndex(mahasiswa) == -1){
            return false;
        }

        return true;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.daftarMahasiswa[this.jumlahMahasiswa] = mahasiswa;
        this.jumlahMahasiswa += 1;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        int index = getIndex(mahasiswa);

        try{
            for(int i = index ; i < this.jumlahMahasiswa ;i ++)
                this.daftarMahasiswa[i] = this.daftarMahasiswa[i + 1];
        }catch (Exception e){}
        this.jumlahMahasiswa -=1;

    }

    public String toString() {
        return this.getNama();
    }

    /*  Getter  */

    public String getKode() {
        return kode;
    }

    public String getNama() {
        return nama;
    }

    public int getSks() {
        return sks;
    }

    public int getKapasitas() {
        return kapasitas;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return daftarMahasiswa;
    }

    public int getJumlahMahasiswa() {
        return jumlahMahasiswa;
    }
}
